const menuBtn = document.querySelector('.menu-btn');
const burguer = document.querySelector('.menu-btn__burguer');

const nav = document.querySelector('.nav');
const menuNav = document.querySelector('.menu-nav');
const menuNavItems = document.querySelectorAll('.menu-nav__item');

let showMenu = false;

menuBtn.addEventListener('click', toggleMenu);

function toggleMenu() {
    if (!showMenu) {
        burguer.classList.add('open');
        nav.classList.add('open');
        menuNav.classList.add('open');
        menuNavItems.forEach(item => item.classList.add('open'));

        showMenu = true;
    } else {
        burguer.classList.remove('open')
        menuNav.classList.remove('open');
        menuNav.classList.remove('open');
        menuNavItems.forEach(item => item.classList.remove('open'));
        showMenu = false;
    }
}


